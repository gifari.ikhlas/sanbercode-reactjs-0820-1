// soal 1
//realease 0
console.log('----realease 0-----');
class Animal{
    constructor(name, legs, cold_blooded){
        this._animalName= name;
        this._animalLegs= legs;
        this._animalBlooded= cold_blooded;
    }
    get animalName() {
        return this._animalName;
    }
    get animalLegs() {
        return this._animalLegs;
    }
    get animalBlooded() {
        return this._animalBlooded;
    }
    set animalName(x) {
        this._animalName = x;
    }
    set animalLegs(x) {
        this._animalLegs = x;
    }
    set animalBlooded(x) {
        this._animalBlooded = x;
    }
}
let myanimal = new Animal('shaun');
myanimal.animalName = "shaun";
myanimal.animalLegs = 4;
myanimal.animalBlooded = false;
console.log(myanimal.animalName);
console.log(myanimal.animalLegs);
console.log(myanimal.animalBlooded);

//realease 1
 console.log('----realease 1-----');
class Ape extends Animal{
    constructor(name, legs, cold_blooded,yell){
        super(name, legs, cold_blooded);
        this._yell = yell;
    }

    get yell(){
        return this._yell;
    }
    set yell(x){
        this._yell = x;

    }
   
}
let sungokong = new Ape ('Kera Sakti');
sungokong.yell= "auooo";
sungokong.legs = 2;
console.log(sungokong.yell);
console.log(sungokong.legs); 

class Frog extends Animal{
    constructor(name, legs, cold_blooded, jump){
        super(name, legs, cold_blooded);
        this._jump = jump;
    }
    get jump(){
        return this._jump;
    }
    set jump(x){
        this._jump = x;
    }
}
let kodok = new Frog('buduk');
kodok.jump="hop hop";
kodok.legs = myanimal.animalLegs;
console.log(kodok.jump);
console.log(kodok.legs);

// soal 2
/* function Clock({ template }) {

    var timer;

    function render() {
        var date = new Date();

        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;

        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;

        var output = template
            .replace('h', hours)
            .replace('m', mins)
            .replace('s', secs);

        console.log(output);
    }

    this.stop = function () {
        clearInterval(timer);
    };

    this.start = function () {
        render();
        timer = setInterval(render, 1000);
    };

}

var clock = new Clock({ template: 'h:m:s' });
clock.start();  */

class Clock{
    constructor(jam, render, stop, start){
         jam = (template)=> {

            let timer;
        render= () => {
            let date = new Date();

            let hours = date.getHours();
            if (hours < 10) hours = '0' + hours;

            let mins = date.getMinutes();
            if (mins < 10) mins = '0' + mins;

            let secs = date.getSeconds();
            if (secs < 10) secs = '0' + secs;

            let output = template
                .replace('h', hours)
                .replace('m', mins)
                .replace('s', secs);

            console.log(output);
        }

        stop =  ()=> {
            clearInterval(timer);
        };

        start =  ()=>{
            render();
            timer = setInterval(render, 1000);
        };
         }
        }
}
let clock = new Clock({ template: 'h:m:s' });

clock.start();