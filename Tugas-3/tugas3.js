//soal 1
var kataPertama ="saya";
var kataKedua= "senang";
var kataKetiga="belajar";
var kataKeempat="javascript";
console.log(kataPertama+' '+kataKedua+' '+kataKetiga+' '+kataKeempat);

//soal 2
var angkaPertama= "1";
var angkaKedua = "2"; 
var angkaKetiga = "3";
var angkaKeempat = "4";
var angkaKelima = "5";
console.log(Number(angkaPertama)+Number(angkaKedua)+Number(angkaKetiga)+Number(angkaKeempat)+Number(angkaKelima));

//soal 3
var kalimat = 'wah javascript itu keren sekali';

var kataPertama = kalimat.substring(0, 3);
var kataKedua= kalimat.substring(4,14);
var kataKetiga= kalimat.substring(14,18); // do your own! 
var kataKeempat= kalimat.substring(19,25); // do your own! 
var kataKelima= kalimat.substring(25,31); 

console.log('Kata Pertama: ' + kataPertama);
console.log('Kata Kedua: ' + kataKedua);
console.log('Kata Ketiga: ' + kataKetiga);
console.log('Kata Keempat: ' + kataKeempat);
console.log('Kata Kelima: ' + kataKelima); 

//soal 4
var nilai=62;
if (nilai >= 80){
    console.log("indexsnya A");
} else if(nilai >= 70 && nilai < 80){
    console.log('indexnya B');
}else if(nilai >= 60 && nilai < 70){
    console.log('indexnya C');
}else if(nilai >= 50 && nilai < 60){
    console.log('indexnya D')
}else{
    console.log('indexnya E')
}

//soal 5
var tanggal = 12;
var bulan = 3;
var tahun = 1997;
switch (bulan) {
    case 3:
        console.log(tanggal+" maret "+tahun);
        break;

    default:
        console.log('input tidak valid')

        break;
}

