// soal 1
console.log('LOOPING PERTAMA');
var pertama = 2;
var jumlahPertama = 20;
while (pertama <= jumlahPertama) {
    console.log(pertama + '- I love coding');
    pertama += 2;
}

console.log('LOOPING KEDUA');
var kedua = 20;
var jumlahKedua = 2;
while (kedua >= jumlahKedua) {
    console.log(kedua + '- I will become a frontend developer');
    kedua -= 2;
}

//soal 2
var nilai=1;
var akhir=20;
for(nilai=nilai; nilai<=akhir; nilai++ ){
    if(nilai%2==0){
        console.log(nilai+'- Berkualitas');
    }else if(nilai%3==0){
        console.log(nilai+'- I love Coding');
    }else if( nilai%2==1){
        console.log(nilai+'- Santai');
    }else{
        console.log('eror');
    }
}

//soal 3
var s='';
for(var i= 0 ; i < 7; i++){
    for (var j = 0; j <= i; j++){
        s +='#';
    }
    s += '\n'
}
console.log(s);

//soal 4
var kalimat = ["saya", "sangat", "senang", "belajar", "javascript"]
console.log(kalimat);

//soal 5
var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
console.log(daftarBuah[4]);
console.log(daftarBuah[0]);
console.log(daftarBuah[2]);
console.log(daftarBuah[3]);
console.log(daftarBuah[1]);