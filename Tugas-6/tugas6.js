// soal 1
  
 const luasLingkaran = (jariJari) => {
    let pi = jariJari % 7 === 0 ? 22 / 7 : 3.14;
    return pi * jariJari * jariJari;
} 
console.log("luas lingkaran dari jari-jari 7 adalah " + luasLingkaran(7));

//soal 2
const kalimat1 = 'saya';
const kalimat2 = 'adalah';
const kalimat3 = 'seorang';
const kalimat4 = 'frontend';
const kalimat5 = 'developer';

const theString =`${kalimat1} ${kalimat2} ${kalimat3} ${kalimat4} ${kalimat5}`;
console.log(theString);

//soal 3
const newFunction = (firstName,lastName)=>{
    firstName : 'gifari'
    lastName : 'ikhlas'
}

//soal 4
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}
const {firstName,lastName,destination,occupation,spell} = newObject;
console.log(firstName, lastName, destination, occupation, spell); 

//soal 5
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

const combined = [...west,...east]
console.log(combined)